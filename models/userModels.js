const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please tell us your name'],
    },
    email: {
        type: String,
        required: [true, 'Please probide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'Default.jpg',
    },
    role: {
        type: String,
        enum: ['user', 'sme', 'pharmacist', 'admin'],
        default: 'user',
    },
    password: {
        type: String,
        required: [true, 'Please provide a password!'],
        minlength: 8,
        //password won't be include when we get the user
        select: false,
    },
    active: {
        type: Boolean,
        default: true,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, "Please confirm your password"],
        validate: {
            validator: function (el) {
                return el === this.password
            },
            message: "Password is not match",
        }
    },
})

    // userSchema.pre('save', async function (next) {
    //     //only run this function if password was actually modified
    //     if (!this.isModified("password")) return next()

    //     //Hash the password with cost of 12
    //     this.password = await bcrypt.hash(this.password, 12)
    //     //Delete passwordconfirm field
    //     this.passwordconfirm = undefined
    //     next()
    // })

    // userSchema.pre('findOneAndUpdate', async function (next) {
    //     const update = this.getUpdate();
    //     if (update.password !== '' && 
    //         update.password !== undefined && update.password == 
    //         update.passwordConfirm) {
    //         //Hash the password with cost of 12
    //         this.getUpdate().password = await bcrypt.hash(update.password, 12)
    //         //Delete passwordconfirm field
    //         update.passwordConfirm = undefined
    //         next()
    //     }else
    //     next()
    // })
    userSchema.pre('save', async function (next) {
        if(!this.isModified('password')) return next()
    
        this.password =  await bcrypt.hash(this.password, 12)
    
        this.passwordConfirm = undefined
        next()
    })
    userSchema.pre('findOneAndUpdate',async function (next){
        const update =this.getUpdate();
        if (update.password !=='' &&
            update.password !== undefined &&
            update.password ==update.passwordConfirm
           ){
            //hash the password with cost of 12 
            this.getUpdate().password =await bcrypt.hash(update.password,12 )
            // // Deleted passwordConfirm field 
            update.passwordConfirm = undefined
            next()
           }else
           next()
    })

    //instance method is available in all document of certain collections
    userSchema.methods.correctPassword = async function (
        candidatePassword,
        userPassword,
    ) {
        return await bcrypt.compare(candidatePassword, userPassword)
    }

const User = mongoose.model('User', userSchema)
module.exports = User