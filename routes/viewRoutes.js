const express = require('express')
const viewsController = require('./../controllers/viewController')
const router = express.Router()


router.get('/', viewsController.getHome)
router.get('/login', viewsController.getLoginForm)
router.get('/signup', viewsController.getSignupForm)




module.exports = router
